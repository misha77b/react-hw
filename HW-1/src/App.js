import React from 'react';
import Modal from './Components/Modal/Modal.jsx';
import Button from './Components/Button/Button.jsx';
import modalWindowDeclarations from './Components/Modal/ModalWindowDeclaration.js';

class App extends React.Component{
  constructor(props) {
    super(props)
    this.state = {
      showModal: false,
      modal: {
        modalId: 0,
        modalColor: 'white',
        modalHeader: 'header',
        modalText: 'text',
        modalbtn1: '',
        modalbtn2: '',
      }
    }
  }

  openModal = (event) => {
    const modalBtnId = event.target.dataset.btnid
    const modalDeclaration = modalWindowDeclarations.find(item => {
      return +item.id === +modalBtnId
    })
    // console.log(modalDeclaration.id);
    this.setState({
      showModal: true,
      modal: {
        modalId: modalDeclaration.id,
        modalColor: modalDeclaration.modalColor,
        modalHeader: modalDeclaration.header,
        modalText: modalDeclaration.text,
        modalBtn1: modalDeclaration.btn1,
        modalBtn2: modalDeclaration.btn2,
      }
    })
  }

  closeModal = () => {
      this.setState({
        showModal: false,
      })
  }

  render() {
    return (
      <div className="App">
        <Button 
          className='open-modal open-first-modal' 
          backgroundColor='rgb(153, 51, 0)'
          onClick={this.openModal} 
          btnid='1' 
          btnText='Open first modal'
        />
        
        <Button 
          className='open-modal open-second-modal' 
          backgroundColor='rgb(204, 51, 51)'
          onClick={this.openModal} 
          btnid='2' 
          btnText='Open second modal'
        />

        {this.state.showModal &&
          <Modal 
            onClick={this.closeModal}
            backgroundColor={this.state.modal.modalColor}
            header={this.state.modal.modalHeader}
            text={this.state.modal.modalText}
            action={
              <>
                <Button 
                  className='modal--true-btn'
                  onClick={() => {alert('Welcome!')}}
                  btnText={this.state.modal.modalBtn1}
                  backgroundColor={this.state.modal.modalColor}
                />
                <Button 
                  className='modal--false-btn'
                  onClick={this.closeModal}
                  btnText={this.state.modal.modalBtn2}
                  backgroundColor={this.state.modal.modalColor}
                />
              </>
            }
          />
        }
      </div>
    )
  }
}

export default App;
