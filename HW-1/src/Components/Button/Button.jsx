import React from "react";

class Button extends React.Component{
    render(){
        return(
            <button className={this.props.className} style={{backgroundColor: this.props.backgroundColor}} onClick={this.props.onClick} data-btnid={this.props.btnid}>
                {this.props.btnText}
            </button>
        )
    }
}

export default Button