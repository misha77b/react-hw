import React from 'react';
import './Modal.scss';
import Button from '../Button/Button.jsx'

class Modal extends React.Component{

    render() {
        return(
            <div className="modal modal--wrapper" onClick={this.props.onClick}>
                <div className="modal--content" style={{backgroundColor: this.props.backgroundColor}} onClick={(e) => e.stopPropagation()}>
                    <div className="modal--header">
                        <Button
                            className='modal-btn modal--closeBtn'
                            onClick={this.props.onClick}
                            btnText='X'
                        />
                        <h2 className='modal--header-text'> {this.props.header} </h2>
                    </div>
                    <div className="modal--body">
                        <p> {this.props.text} </p>
                    </div>
                    <div className="modal--footer">
                        {this.props.action}
                    </div>
                </div>
            </div>  
        )
    }
}

export default Modal