const modalWindowDeclarations = [
    {
      id: '1',
      modalColor: 'rgba(165, 42, 42, 0.9)',
      header: "Do you want to delete this file",
      text: 'Once you delete this file, it won\'t be possible to undo this action. Are you sure you want to delete it?',
      btn1: 'Ok',
      btn2 :'Cancel',
    },
    {
      id: '2',
      modalColor: 'rgba(204, 51, 51, 0.8)',
      header: "One more chance, think twice",
      text :'This time, there would be no way back, this is your last chance!',
      btn1: 'Continue',
      btn2 :'Exit',
    }
]

export default modalWindowDeclarations