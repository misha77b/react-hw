import React from "react";
import './Button.scss';
import PropTypes from 'prop-types';

class Button extends React.Component{
    render(){
        return(
            <button className={this.props.className} onClick={this.props.onClick} data-btnid={this.props.btnid}>
                {this.props.text}
            </button>
        )
    }
}

export default Button


Button.propTypes = {
    className: PropTypes.string,
    onclick: PropTypes.func,
    btnid: PropTypes.string,
    text: PropTypes.string.isRequired
}
Button.defaultProps = {
    text: null
}