import React from 'react';
import PropTypes from 'prop-types';
import Button from '../Button/Button';
import './Cards.scss';
import StarSvg from '../Icons/StarSvg';

class Card extends React.Component {
    constructor(props){
        super(props);
        this.props = props;
    }
    render() {
        const starSvg = true;
        return(
            <div className='card-wrapper' data-card-id={this.props.cardId} key={this.props.keyId}>
                {starSvg ? (<StarSvg
                    onClick={this.props.onStarClick}
                    id={this.props.cardId}
                    className="add-to-favoutites-btn"
                />) : !null}
                <img className='product-img' src={this.props.img} alt="product-img" />
                <h2 className='product-name'>{this.props.name}</h2>
                <div className='product-info'>
                    <span className='product-color'> 
                        <strong>Color:</strong>  {this.props.color} 
                    </span> 
                    <span className='product-price'>
                        <strong>Price:</strong> {this.props.price} 
                    </span>
                </div>
                <Button 
                    text={this.props.textButton}
                    onClick={this.props.onClick}
                    className="add-to-cart-btn"
                    btnid={this.props.btnid}
                />
            </div>
        )
    }
}

export default Card

Card.propTypes = {
    id: PropTypes.string,
    key: PropTypes.string,
    img: PropTypes.string,
    name: PropTypes.string,
    price: PropTypes.string,
    color: PropTypes.string,
    onclick: PropTypes.func
}
Card.defaultProps = {
    id: null,
    onClick: null
}