import React from 'react';
import './Modal.scss';
import Button from '../Button/Button.jsx';
import PropTypes from 'prop-types';

class Modal extends React.Component{

    render() {
        return(
            <div className="modal modal--wrapper" onClick={this.props.onClick}>
                <div className="modal--content" onClick={(e) => e.stopPropagation()}>
                    <div className="modal--header">
                        <Button
                            className='modal-btn modal--closeBtn'
                            onClick={this.props.onClick}
                            text={this.props.closeModal}
                        />
                        <h2 className='modal--header-text'> {this.props.header} </h2>
                    </div>
                    <div className="modal--body">
                        <p> {this.props.text} </p>
                    </div>
                    <div className="modal--footer">
                        <Button
                            className='agree-btn'
                            onClick={this.props.onAgreeClick}
                            text={this.props.agreeBtn}
                        />
                        <Button
                            className='refuse-btn'
                            onClick={this.props.onClick}
                            text={this.props.refuseBtn}
                        />
                    </div>
                </div>
            </div>  
        )
    }
}

export default Modal

Modal.propTypes = {
    onClick: PropTypes.func,
    header: PropTypes.string,
    text: PropTypes.string,
    className: PropTypes.string
}

Modal.defaultProps = {
    closeModal: 'X',
}