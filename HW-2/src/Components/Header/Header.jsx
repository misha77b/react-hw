import React from "react";
import "./Header.scss"
import logoImg from "./logo-img.png"

class Header extends React.Component {
    render() {
        return(
            <div className="header">
                <img src={logoImg} style={{width: '140px', height: '85px'}} alt="store-logo-img" />
                <h1 className="header-text"> Audi Store </h1>
            </div>
        )
    }
}

export default Header