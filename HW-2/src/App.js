import React from 'react';
import Modal from './Components/Modal/Modal.jsx';
import Card from './Components/Card/Card.jsx';
import Header from './Components/Header/Header.jsx';

class App extends React.Component{
  constructor(props) {
    super(props)
    this.state = {
      showModal: false,
      products: [],
    }
  }

  componentDidMount() {
    fetch('./storage.json')
     .then(res => res.json())
     .then(res => {
       this.setState({ ...this.state, products: res, })
     })
     if (!localStorage.getItem("Favorites")) localStorage.setItem("Favorites", "[]");     
     if (!localStorage.getItem("Products cart")) localStorage.setItem("Products cart", "[]");
  }

  openModal = (event) => {
    this.setState({
      ...this.state, 
      showModal: !this.state.showModal,
    })
    this.elementId = event.target.dataset.btnid;
  }
  
  addToCart = () => {
    const productsInCart = JSON.parse(localStorage.getItem("Products cart"));
    const addProductToCart = productsInCart.map((product) => product);
    addProductToCart.push(this.elementId);
    localStorage.setItem("Products cart", JSON.stringify(addProductToCart));
    this.setState({
      showModal: false,
    });
  }
  
  addToFavourite = (event) => {
    const favouriteProducts = JSON.parse(localStorage.getItem("Favorites"));
    const newFavouriteProduct = favouriteProducts.map((car) => car);
    if (!newFavouriteProduct.includes(event.target.parentElement.id)) {
      newFavouriteProduct.push(event.target.parentElement.id);
      localStorage.setItem("Favorites", JSON.stringify(newFavouriteProduct));
      event.target.classList.toggle("active-star");
    } else if (newFavouriteProduct.includes(event.target.parentElement.id)) {
      const index = newFavouriteProduct.indexOf(event.target.parentElement.id);
      newFavouriteProduct.splice(index, 1);
      localStorage.setItem("Favorites", JSON.stringify(newFavouriteProduct));
      event.target.classList.toggle("active-star");
    }
  }

  closeModal = () => {
    this.setState({
      showModal: false,
    });
  }

  render() {
    return (
      <div className="app" style={{display: 'inline-block'}}>
        <Header />
        <div className='products'>
          {this.state.products.map((product) => {
            return <>
              <Card 
                cardId={product.id}
                keyId={product.id}
                img={product.img}
                name={product.name}
                price={product.price}
                color={product.color}
                textButton="Add to cart"
                onClick={this.openModal}
                btnIdKey={product.id}
                btnid={product.id}
                onStarClick={this.addToFavourite}
              />
            </>
          })}
        </div>
        
        {
          this.state.showModal &&
          <Modal
            onAgreeClick={this.addToCart}
            onClick={this.closeModal}
            header="Add car"
            text='Are you sure you want to add this item to cart?'
            agreeBtn='Yes'
            refuseBtn='No'
          />
        }
      </div>
    )
  }
}

export default App;
